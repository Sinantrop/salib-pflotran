import subprocess
from SALib.sample import saltelli
from SALib.analyze import sobol
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def run_model(H_con, HCO3_con, Ca_con):
    filename = 'tran.in'
    f = open(filename,'w')

    model = """
    SIMULATION
      SIMULATION_TYPE SUBSURFACE
      PROCESS_MODELS
        SUBSURFACE_FLOW flow
          MODE RICHARDS
        /
        SUBSURFACE_TRANSPORT transport
          MODE GIRT
        /
      /
    END
    
    SUBSURFACE
    
    #=========================== numerical methods ================================
    NUMERICAL_METHODS FLOW
    
      LINEAR_SOLVER
        SOLVER DIRECT
      /
    
    END
    
    NUMERICAL_METHODS TRANSPORT
    
      LINEAR_SOLVER
        SOLVER DIRECT
      /
    
    END

    #=========================== chemistry ========================================
    CHEMISTRY
      PRIMARY_SPECIES
        H+
        HCO3-
        Ca++
      /
      SECONDARY_SPECIES
        OH-
        CO3--
        CO2(aq)
        CaCO3(aq)
        CaHCO3+
        CaOH+
      /
      PASSIVE_GAS_SPECIES
        CO2(g)
      /
      MINERALS
        Calcite
      /
      MINERAL_KINETICS
        Calcite
          RATE_CONSTANT 1.d-6 mol/m^2-sec
        /
      /
      DATABASE hanford.dat
      LOG_FORMULATION
    #  OPERATOR_SPLITTING
      ACTIVITY_COEFFICIENTS TIMESTEP
      OUTPUT
        PH
        TOTAL
        ALL
      /
    END
    
    #=========================== solver options ===================================
    
    
    #=========================== discretization ===================================
    GRID
      TYPE STRUCTURED
      NXYZ 100 1 1
      BOUNDS
        0.d0 0.d0 0.d0
        100.d0 1.d0 1.d0
      /
    END
    
    #=========================== fluid properties =================================
    FLUID_PROPERTY
      DIFFUSION_COEFFICIENT 1.d-9
    END
    
    #=========================== material properties ==============================
    MATERIAL_PROPERTY soil1
      ID 1
      POROSITY 0.25d0
      TORTUOSITY 1.d0
      PERMEABILITY
        PERM_ISO 1.d-12
      /
      CHARACTERISTIC_CURVES default
    END
    
    #=========================== characteristic curves ============================
    CHARACTERISTIC_CURVES default
      DEFAULT
    END
    
    #=========================== output options ===================================
    OUTPUT
      TIMES y 5. 10. 15. 20.
      FORMAT TECPLOT POINT
      VELOCITY_AT_CENTER
    END
    
    #=========================== times ============================================
    TIME
      FINAL_TIME 25.d0 y
      INITIAL_TIMESTEP_SIZE 1.d0 h
      MAXIMUM_TIMESTEP_SIZE 2.5d-1 y
    END
    
    #=========================== regions ==========================================
    REGION all
      COORDINATES
        0.d0 0.d0 0.d0
        100.d0 1.d0 1.d0
      /
    END
    
    REGION west
      FACE WEST
      COORDINATES
        0.d0 0.d0 0.d0
        0.d0 1.d0 1.d0
      /
    END
    
    REGION east
      FACE EAST
      COORDINATES
        100.d0 0.d0 0.d0
        100.d0 1.d0 1.d0
      /
    END
    
    #=========================== flow conditions ==================================
    FLOW_CONDITION initial_pressure
      TYPE
        PRESSURE DIRICHLET
      /
      PRESSURE 201325.d0
    END
    
    FLOW_CONDITION inlet_flux
      TYPE
        FLUX NEUMANN
      /
      FLUX 1.d0 m/y
    END
    
    #=========================== transport conditions =============================
    TRANSPORT_CONDITION background_conc
      TYPE ZERO_GRADIENT
      CONSTRAINT_LIST
        0.d0 initial_constraint
      /
    END
    
    TRANSPORT_CONDITION inlet_conc
      TYPE DIRICHLET_ZERO_GRADIENT
      CONSTRAINT_LIST
        0.d0 inlet_constraint
      /
    END
    
    #=========================== constraints ======================================
    CONSTRAINT initial_constraint
      CONCENTRATIONS
        H+     1.d-8      F
        HCO3-  1.d-3      G  CO2(g)
        Ca++   5.d-4      M  Calcite
      /
      MINERALS
        Calcite 1.d-5 1.d0 m^2/m^3
      /
    END
    
    CONSTRAINT inlet_constraint
      CONCENTRATIONS
        H+     """+ str(H_con) + """       P
        HCO3-  """+ str(HCO3_con) + """    T
        Ca++   """+ str(Ca_con) + """      Z
      /
    END
    
    #=========================== condition couplers ===============================
    # initial condition
    INITIAL_CONDITION
      FLOW_CONDITION initial_pressure
      TRANSPORT_CONDITION background_conc
      REGION all
    END
    
    BOUNDARY_CONDITION outlet
      FLOW_CONDITION initial_pressure
      TRANSPORT_CONDITION background_conc
      REGION east
    END
    
    BOUNDARY_CONDITION inlet
      FLOW_CONDITION inlet_flux
      TRANSPORT_CONDITION inlet_conc
      REGION west
    END
    
    #=========================== stratigraphy couplers ============================
    STRATA
      REGION all
      MATERIAL soil1
    END
    
    
    END_SUBSURFACE
    """

    f.write(model)
    f.close()

    subprocess.run(["bash", "-c", "mpirun -n 1 /home/ondro/pflotran/src/pflotran/pflotran -input_prefix tran"])
    
    pH_data = pd.read_csv("tran-005.tec", skiprows=3, header=None, delim_whitespace=True)
    pH_data = pH_data.iloc[53]
    pH_data = pH_data.loc[5]
    #pH_data = float(pH_data)
    return pH_data 

pH_data = run_model(5, 1e-3, 1e-5)
print(pH_data)

problem = {
    'num_vars': 3,
    'names': ['H_con', 'HCO3_con', 'Ca_con'],
    'bounds': [[5., 8.],
               [1.e-3, 2.e-3],
               [1.e-2, 2]]
}

parameter_values = saltelli.sample(problem, 2**6)

pH_array = np.zeros([parameter_values.shape[0], 1])
for i, param in enumerate(parameter_values):
    pH_array[i] = run_model(param[0], param[1], param[2])

SI = sobol.analyze(problem, pH_array[:,0], print_to_console=True)

SI.plot()






